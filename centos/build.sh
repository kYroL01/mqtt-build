#! /bin/bash
#
# build.sh
# Copyright (C) 2019 grumvalski <grumvalski@hal>
#
# Distributed under terms of the MIT license.
#
set -e

LIBMQTT_VERSION="1.3.1"

SCRIPT_HOME="$(dirname "$(realpath "$0")")"

yum install -y epel-release

CENTOS_RELEASE="$(cat /etc/os-release |grep VERSION_ID|cut -d "=" -f2|sed -e 's/"//g')"

if [ "$CENTOS_RELEASE" == "8" ]; then
  dnf install -y 'dnf-command(config-manager)' 
  yum config-manager --set-enabled PowerTools
  yum update -y
fi 

yum install -y cmake doxygen gcc git graphviz make openssl-devel rpm-build

git clone https://github.com/eclipse/paho.mqtt.c.git /tmp/paho.mqtt.c

mkdir -p /root/rpmbuild/SOURCES

cp ${SCRIPT_HOME}/paho-c.spec /tmp/paho.mqtt.c/dist/

cd /tmp/paho.mqtt.c
tar --transform="s/\./libpahomqttc-${LIBMQTT_VERSION}/" -cf /root/rpmbuild/SOURCES/v${LIBMQTT_VERSION}.tar.gz --exclude=./build.paho --exclude=.git --exclude=*.bz ./ --gzip

rpmbuild -bb dist/paho-c.spec

cp /root/rpmbuild/RPMS/*/libpahomqttc-${LIBMQTT_VERSION}* /${SCRIPT_HOME}
cp /root/rpmbuild/RPMS/*/libpahomqttc-devel-${LIBMQTT_VERSION}* /${SCRIPT_HOME}

